import Vue from 'vue'
import Router from 'vue-router'

import HelloWorld from '../components/HelloWorld.vue'

Vue.use(Router)

export default new Router({
	routes:[
		{
		  path: '/',
		  name: '主页',
		  component: HelloWorld
		},
		{
		  path: '/index',
		  name: '主页1',
		  component: HelloWorld
		},
	]
})
