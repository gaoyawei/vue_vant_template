import Vue from 'vue'  //加载vue
import App from './App' //加载入口文件
import router from './router/index.js' //加载路由配置文件
import Vant from 'vant';	//加载Vant文件
import Public from './shujudata/public.js' //加载公用js方法文件
import 'vant/lib/vant-css/index.css';	//加载Vant样式文件
import './assets/index.less'	//	加载less文件
import Axios from './shujudata/api.js'; //加载接口文件

Vue.use(Vant);

Vue.prototype.$http= Axios;
Vue.prototype.$public = Public;
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
